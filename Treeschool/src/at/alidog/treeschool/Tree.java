package at.alidog.treeschool;

import at.alidog.treeschool.FertilizeMethod;
import at.alidog.treeschool.CultivationArea;

public abstract class Tree {

	private int areaID;
	private int treeID;
	private FertilizeMethod fertilizeID;
	private float maxSize;
	private float maxDiameter;
	
	public Tree(int areaID, int treeID, FertilizeMethod fertilizeID, float maxSize, float maxDiameter) {
		this.areaID = areaID;
		this.treeID = treeID;
		this.fertilizeID = fertilizeID;
		this.maxSize = maxSize;
		this.maxDiameter = maxDiameter;
	}



	
	
}
