package at.alidog.treeschool;

public class main {

	public static void main(String[] args) {
		
		FertilizeMethod TopGreen = new TopGreenFertilizer();
		FertilizeMethod SuperGrow = new SuperGrowFertilizer();
		Conifer conifer1 = new Conifer(1, 1, TopGreen, 400, 30);
		Conifer conifer2 = new Conifer(1, 2, TopGreen, 400, 30);
		Conifer conifer3 = new Conifer(2, 2, TopGreen, 400, 30);
		Leaftree leaftree1 = new Leaftree(1, 1, SuperGrow, 400, 30);

		TopGreen.doFertilize();
		SuperGrow.doFertilize();
	}

}
