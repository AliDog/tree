package at.alidog.treeschool;

public class TopGreenFertilizer implements FertilizeMethod {

	@Override
	public void doFertilize() {
		System.out.println("The trees have been fertilised with the Top Green Method");
		
	}

}
